echo off

rem this will read properties from deploy.properties file.
For /F "tokens=1* delims==" %%A IN (deploy.properties) DO (
	IF "%%A"=="CLOUD_SEER_PATH" set cloud_seer_path=%%B
	IF "%%A"=="SOLUTION_NAME" set solution_name=%%B
	)
rem echo %cloud_seer_path%
rem echo %solution_name%

rem copy files from unzipped solution archive to cloudseer/web-inf/lib.
robocopy ./libs %cloud_seer_path%/web-inf/copylib /E /XO *

rem copy files from application webcontent to cloudseer.
robocopy ./%solution_name% %cloud_seer_path%/%solution_name% /E /XO *

rem copy files from unzipped solution archive to cloudseer/web-inf/lib.
robocopy ./resources/config %cloud_seer_path%/web-inf/classes/config/%solution_name% /E /XO *