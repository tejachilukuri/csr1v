var gulp = require('gulp'),
  uglify = require('gulp-uglify'),
  pump = require('pump'),
  gulpTS = require('gulp-typescript'),
  less = require('gulp-less'),
  path = require('path'),
  del = require('del'),
  rename = require('gulp-rename'),
  runSequence = require('run-sequence'),
  fs = require('fs'),
  sourcemaps = require('gulp-sourcemaps'),
  rootFolder = path.join(__dirname),
  srcFolder = path.join(rootFolder, 'src'),
  //jsFolder = path.join(rootFolder, 'js'),
  appsrcFolder = path.join(srcFolder, 'solution'),
  appdevsrcFolder = path.join(srcFolder, 'solution', 'solution-launcher'),
  node_modules_folder = path.join(rootFolder, 'node_modules'),
  buildFolder = path.join(rootFolder, 'build'),
  themesfolder = path.join(srcFolder, 'themes'),
  distFolder = path.join(rootFolder, 'build', 'dist', 'webcontent'),
  appdestFolder = path.join(distFolder, 'solution');
  appdevdistFolder = path.join(distFolder, 'solution', 'solution-launcher');
  

const themesConfig = require(themesfolder + "/themes.config.json");

const externals = require("./src/systemjs.externals");
gulp.task("node_modules", () => {
  var files = externals.externals.map((file) => {
    var result = node_modules_folder + '\\' + file
    if (!result.endsWith('.js')) {
      result = result + '/**'
    }
    return result;
  });
  return gulp.src(files, {
      base: './'
    })
    .pipe(gulp.dest(distFolder));
});

gulp.task('tsc', function () {
  var tsProject = gulpTS.createProject("tsconfig.json");
  return tsProject.src()
    .pipe(tsProject())
    .pipe(gulp.dest(distFolder));
});

gulp.task('compress', function (cb) {
 return pump([
      gulp.src(`${appdestFolder}/**/*.js`),
      uglify(),
      gulp.dest(appdestFolder)
    ]
  );
});


gulp.task('copy:main-app', function () {
  return gulp.src([`${appsrcFolder}/**/*`, `!${srcFolder}/**/*.ts`, `!${appsrcFolder}/**/*.js`, `!${srcFolder}/**/*.map`])
    .pipe(gulp.dest(appdestFolder));
});

gulp.task('copy:dev-app', function () {
  return gulp.src([`${appdevsrcFolder}/**/*`, `!${appdevsrcFolder}/**/*.ts`, `!${appdevsrcFolder}/**/*.map`])
    .pipe(gulp.dest(appdevdistFolder));
});

gulp.task('remove:appsettings', function () {
  return del[(`${appdevdistFolder}/app.settings.js`)]
});

gulp.task('useprod:appsettingsprod', function () {
  return gulp.src(`${appdevdistFolder}/app.settings.prod.js`)
    .pipe(rename('app.settings.js'))
    .pipe(gulp.dest(appdevdistFolder));
});

gulp.task('remove:appsettingsprod', function () {
  return del[(`${appdevdistFolder}/app.settings.prod.js`)]
});

gulp.task('copy:i18n', function () {
  return gulp.src([`${srcFolder}/i18n/**/*`, `!${srcFolder}/**/*.ts`])
    .pipe(gulp.dest(`${distFolder}/i18n`));
});

gulp.task('copy:js', function () {
	  return gulp.src([`${rootFolder}/js/**/*`])
	    .pipe(gulp.dest(`${distFolder}/js`));
	});

gulp.task('copy:themes', function () {
  return gulp.src([`${srcFolder}/themes/css/**`, `!${srcFolder}/**/*.ts`])
    .pipe(gulp.dest(`${distFolder}/themes/css/`));
});

gulp.task('copy:assets', function () {
  return gulp.src([`${srcFolder}/assets/**/*`])
    .pipe(gulp.dest(`${distFolder}/assets`));
});

gulp.task('copy:lib', function () {
  return gulp.src([`${rootFolder}/@apporchid/**/*`, `!${rootFolder}/**/*.ts`])
    .pipe(gulp.dest(`${distFolder}/@apporchid`));
});

gulp.task('copy:thirdparty', function () {
  return gulp.src([`${srcFolder}/thirdparty/**/*`, `!${srcFolder}/**/*.ts`])
    .pipe(gulp.dest(`${distFolder}/thirdparty`));
});

gulp.task('copy:utils', function () {
  return gulp.src([`${srcFolder}/utils/**/*`, `!${srcFolder}/**/*.ts`])
    .pipe(gulp.dest(`${distFolder}/utils`));
});


gulp.task('less', function () {
  return gulp.src(`${srcFolder}/**/*.less`)
    .pipe(less())
    .pipe(gulp.dest(distFolder));
});

gulp.task('less:build', function () {
  let theme = "default";
  if (themesConfig['theme']) {
    theme = themesConfig['theme'];
  }
  return gulp.src(`${srcFolder}/themes/less/default/${theme}.theme.less`)
    .pipe(less())
    .pipe(rename('index.css'))
    .pipe(gulp.dest(`${srcFolder}/themes/css/`));
});

gulp.task('copy:src', function () {
  return gulp.src([`${srcFolder}/systemjs-angular-loader.js`,
      `${srcFolder}/index.html`
    ])
    .pipe(gulp.dest(`${distFolder}`));
});

gulp.task('copy:systemProd', function () {
  return gulp.src(`${srcFolder}/systemjs.config.prod.js`)
    .pipe(rename('systemjs.config.js'))
    .pipe(gulp.dest(`${distFolder}`));
});

gulp.task('less:watch', function () {
  gulp.watch(`${srcFolder}/**/*.less`, ['less']);
});

gulp.task('build', function () {
  del(distFolder).then((res) => {
    console.log('deleted ' + res);
    runSequence(
      'tsc',
      'less:build',
      'copy:main-app',
      'copy:dev-app',
      'remove:appsettings',
      'useprod:appsettingsprod',
      'remove:appsettingsprod',
      'compress',
      'copy:i18n',
      'copy:themes',
      'copy:assets',
      'copy:lib',
      'copy:thirdparty',
      'copy:js',
      'copy:utils',
      'copy:src',
      'node_modules',
      'copy:systemProd',
      function (err) {
        if (err) {
          console.log('ERROR:', err.message);
          del(distFolder);
        } else {
          console.log('Compilation finished succesfully');
        }
      });
  });
});
