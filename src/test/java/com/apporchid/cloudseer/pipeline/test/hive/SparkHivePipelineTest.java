package com.apporchid.cloudseer.pipeline.test.hive;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasource.db.RelationalDBDatasource;
import com.apporchid.cloudseer.datasource.db.RelationalDBDatasourceProperties;
import com.apporchid.common.PlatformApplication;
import com.apporchid.foundation.pipeline.EPipelineRunnerType;

@ActiveProfiles("test")
@SpringBootTest(classes = { PlatformApplication.class })
public class SparkHivePipelineTest extends BasePipelineTest {

	@Test(dataProvider = "hiveSourcePostgresSinkCluster")
	public void hiveSourcePostgresSinkClusterPipeline(DatasourceTaskType sourceTask, DatasinkTaskType sinkTask)
			throws Exception {
		createAndRunSparkPipeline("Hive Source And Postgres Sink with Spark remote cluster",
				EPipelineRunnerType.SPARK_REMOTE, sourceTask, sinkTask);
	}

	@DataProvider(name = "hiveSourcePostgresSinkCluster")
	protected Object[][] hiveSourcePostgresSinkCluster() {

		DatasourceTaskType hivedatasourceTask = new DatasourceTaskType.Builder().name("Read from HIVE")
				.datasourceType(RelationalDBDatasource.class)
				.datasourcePropertiesType(RelationalDBDatasourceProperties.class)
				.property(RelationalDBDatasourceProperties.EProperty.sqlDatasourceName.name(), "hiveTestDB")
				//.property(RelationalDBDatasourceProperties.EProperty.sqlQuery.name(), query)
				.property(RelationalDBDatasourceProperties.EProperty.tableName.name(), "ao_test.employ_1530175212832").build();

		DatasinkTaskType postgresDatasinkTask = new DatasinkTaskType.Builder().name("Write to Postgres")
				.datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
				.property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "postgresDB")
				.property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "temp_hive_" + System.currentTimeMillis()).build();

		return new Object[][] { { hivedatasourceTask, postgresDatasinkTask } };
	}

}
