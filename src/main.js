"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("@angular/core");
var main_module_1 = require("./solution/solution-launcher/main.module");
var app_settings_1 = require("./solution/solution-launcher/app.settings");
if (app_settings_1.AppSettings.PRODUCTION) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(main_module_1.MainModule)
    .catch(function (err) { return console.log(err); });
