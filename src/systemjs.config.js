/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      'css': 'npm:systemjs-plugin-css/css.js',
      'text': 'npm:systemjs-plugin-text/text.js',
      'app': 'solution',

      // angular bundles
      '@angular/animations': 'npm:@angular/animations/bundles/animations.umd.js',
      '@angular/animations/browser': 'npm:@angular/animations/bundles/animations-browser.umd.js',
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/common/http': 'npm:@angular/common/bundles/common-http.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser/animations': 'npm:@angular/platform-browser/bundles/platform-browser-animations.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/router/upgrade': 'npm:@angular/router/bundles/router-upgrade.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
      '@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
      '@angular/upgrade/static': 'npm:@angular/upgrade/bundles/upgrade-static.umd.js',
      'rxjs': 'npm:rxjs',
      '@apporchid/vulcanuxcore': 'npm:@apporchid/vulcanuxcore/vulcanuxcore.umd.js',
      '@apporchid/vulcanuxcontrols': 'npm:@apporchid/vulcanuxcontrols/vulcanuxcontrols.umd.js',
      '@apporchid/vulcanuxhighchart': 'npm:@apporchid/vulcanuxhighchart/vulcanuxhighchart.umd.js',
      'highcharts': 'npm:highcharts',
      'angular2-highcharts': 'npm:angular2-highcharts',
      "@apporchid/vulcanuxgooglemaps": 'npm:@apporchid/vulcanuxgooglemaps/vulcanuxgooglemaps.umd.js',
      "@apporchid/vulcanuxesrimaps": 'npm:@apporchid/vulcanuxesrimaps/vulcanuxesrimaps.umd.js',
      'esri-loader': 'npm:esri-loader',
      'atmosphere.js': 'npm:atmosphere.js/lib/atmosphere.js',
      'angular2-esri-loader': 'npm:angular2-esri-loader',
      // other libraries
      'tslib': 'npm:tslib/tslib.js',
      'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js',
        meta: {
          './*.js': {
            loader: 'systemjs-angular-loader.js'
          },
          '*.css': { loader: 'css' }
        }
      },
      rxjs: {
        defaultExtension: 'js',
        main: 'Rx.js'
      },
      'angular2-highcharts': {
        defaultExtension: 'js'
      },
      highcharts: {
        defaultExtension: 'js'
      },
      'angular2-esri-loader': {
        defaultExtension: 'js'
      },
      'esri-loader': {
        main:'./dist/esri-loader.js',
        defaultExtension: 'js'
      },
    },
    meta: {
      '*.css': { loader: 'css' }
    }
  });
})(this);
//Cache Busting
var systemLocate = System.locate;
System.locate = function (load) {
  var System = this; // its good to ensure exact instance-binding
  return Promise.resolve(systemLocate.call(this, load)).then(function (address) {

    if (address.endsWith('.html.js')) {
      //This feels a little hacky, not sure how to allow html files in the main config.
      address = address.slice(0, -3);
    }

    return address + System.app_version_code;
  });
}
System.app_version_code = '?v=@0.1.0';