"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var AppSettings = (function () {
    function AppSettings() {
    }
    Object.defineProperty(AppSettings, "SOLUTION_CONFIG_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'mock-api/solution.config.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_DETAILS_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'mock-api/app-details.data.json';
            // return 'api/appstore/application';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_LANDING_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'mock-api/appstore-config.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "REVIEW_SUBMIT_URL", {
        get: function () {
            return 'api/appstore/application/comment';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_HEADER_CONFIG_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/appstore-header.config.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SOLUTIONS_DATA_URL", {
        get: function () {
            if (AppSettings.MOCK) {
                return AppSettings.BASE_URL_DEV + 'mock-api/solutions-data.json';
            }
            return 'api/appstore/getAllSolutions';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APPS_DATA_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'mock-api/apps-data.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SEARCH_RESULTS_DATA_URL", {
        get: function () {
            if (AppSettings.MOCK) {
                return 'mock-api/search-results.data.json';
            }
            return 'api/appstore/applicationsSearch/';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "RATING_SUBMIT_URL", {
        get: function () {
            return 'api/appstore/application/rating';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_FOOTER_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/footer.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_MULTIVIEW_URL", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/multiview.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_SOLUTIONCOMPOSER_FORM", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/solution-composer-form.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_SOLUTIONCOMPOSER_AVATARS", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/solution-composer-avatars.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_SOLUTIONREQUEST", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/solution-request.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_SOLUTION_SAVE", {
        get: function () {
            return 'api/vux/solution/publish';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "APP_SOLUTION_EDIT", {
        get: function () {
            return 'api/vux/solution/updateSolution';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "ADD_APP_TO_CART_URL", {
        get: function () {
            return 'api/appstore/addAppToCart';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "REMOVE_APP_FROM_CART_URL", {
        get: function () {
            return 'api/appstore/removeAppFromCart';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "GET_SOLUTION_BY_ID", {
        get: function () {
            return 'api/appstore/solution/';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "GET_VUX_SOLUTION_BY_ID", {
        get: function () {
            if (AppSettings.MOCK) {
                return AppSettings.BASE_URL_DEV + 'mock-api/solution.config.json';
                ;
            }
            return 'api/vux/solution/';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "GET_ALLAPPS_BY_USER", {
        get: function () {
            return 'api/appstore/getAppsInCart';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "ADD_APPS_TO_SOLUTION", {
        get: function () {
            return 'api/appstore/application/updateAppsToSolution';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "GET_RELATEDAPPS", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'mock-api/related-apps.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "GET_SEARCH_SUGGESTIONS", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'mock-api/search-suggestions.json';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "TOPTRENDING_DATA", {
        get: function () {
            if (AppSettings.MOCK) {
                return AppSettings.BASE_URL_DEV + 'mock-api/toptrending.json';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    AppSettings.appDetailPage = function (solutionId, pageId) {
        return 'api/vux/solutionPage/' + solutionId + '/' + pageId;
    };
    Object.defineProperty(AppSettings, "GET_SETTINGS", {
        get: function () {
            return AppSettings.BASE_URL_DEV + 'assets/api/settings.json';
        },
        enumerable: true,
        configurable: true
    });
    AppSettings.intitialize = function () {
        vulcanuxcore_1.AppSettings.LOGIN_APP_CONFIG_URL = AppSettings.BASE_URL_DEV + 'assets/api/login.config.json';
        vulcanuxcore_1.AppSettings.BASE_URL_MAIN = 'http://localhost:8080/';
        vulcanuxcore_1.AppSettings.getSolutionResourcePath = function (solutionName, fileName) {
            if(solutionName === 'appstore' || solutionName ==='samples') {
                return 'solution/solution-launcher/solutions/' + solutionName + '/' + fileName;
            } 
            return 'solution/' + fileName;
        };
        if (AppSettings.MOCK) {
            vulcanuxcore_1.AppSettings.BYPASS_LICENSE = true;
            vulcanuxcore_1.AppSettings.NEXT_ROUTE = 'appstore';
            vulcanuxcore_1.AppSettings.TEST_LOGIN = true;
            vulcanuxcore_1.AppSettings.SIGN_IN_PATH = AppSettings.BASE_URL_DEV + "mock-api/sigin-mock.data.json";
            vulcanuxcore_1.AppSettings.getLoadMicroAppURL = function () {
                console.log("getLoadMicroAppURL");
                return AppSettings.BASE_URL_DEV + 'mock-api/micro_app.json';
            };
        }
    };
    AppSettings.getAppDetailsURL = function (appId) {
        return AppSettings.APP_DETAILS_URL + '/' + appId + '/' + true;
    };
    AppSettings.getSolutionById = function (id) {
        return AppSettings.GET_SOLUTION_BY_ID + '/' + id;
    };
    AppSettings.getVuxSolutionById = function (id) {
        if (AppSettings.MOCK) {
            return AppSettings.BASE_URL_DEV + 'mock-api/solution.config.json';
        }
        return AppSettings.GET_VUX_SOLUTION_BY_ID + id;
    };
    Object.defineProperty(AppSettings, "APP_RESOURCES_PATH", {
        get: function () {
            return 'vux-resources/applications/';
        },
        enumerable: true,
        configurable: true
    });
    AppSettings.getImageURL = function (appId, image) {
        return AppSettings.BASE_URL_DEV + 'assets/images/thumbnails/' + image;
    };
    AppSettings.getResourceURL = function (appId, type, image) {
        // return DefaultSettings.BASE_URL_MAIN + 'assets/' + appId + '/' + type + '/' + image;
        return vulcanuxcore_1.AppSettings.BASE_URL_MAIN + AppSettings.APP_RESOURCES_PATH + appId + '/' + type + '/' + image;
    };
    AppSettings.getAddAppToCartURL = function (appId) {
        return AppSettings.ADD_APP_TO_CART_URL + '/' + appId;
    };
    AppSettings.getRemoveAppFromCartURL = function (appId) {
        return AppSettings.REMOVE_APP_FROM_CART_URL + '/' + appId;
    };
    Object.defineProperty(AppSettings, "SETTINGS_PAGE_APPS_URL", {
        get: function () {
            return 'api/appstore/getApplicationsInfoOfUser';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SETTINGS_PAGE_SOLUTIONS_URL", {
        get: function () {
            return 'api/appstore/getAllSolutions';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SETTINGS_PAGE_SOLUTION_EDIT", {
        get: function () {
            return 'api/appstore/getAllSolutions';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SETTINGS_PAGE_SOLUTION_REMOVE", {
        get: function () {
            return 'api/vux/solution/delete';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SETTINGS_PAGE_SOLUTION_DEFAULT", {
        get: function () {
            return 'api/appstore/solution/setDefaultSolution';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "CLEAR_ALL_APPS", {
        get: function () {
            return 'api/appstore/application/clearAllAppsFromCart';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "SWAGGER_URL", {
        get: function () {
            return 'http://localhost:8080/swagger/swagger-ui/index.html';
        },
        enumerable: true,
        configurable: true
    });
    AppSettings.solutionComposerPage = function (solutionId, pageId) {
        return 'api/vux/solutionPage/' + solutionId + '/' + pageId;
    };
    Object.defineProperty(AppSettings, "UPLOAD_ICON", {
        get: function () {
            return 'api/appstore/uploadImage';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "RESOURCESPATH", {
        get: function () {
            return vulcanuxcore_1.AppSettings.BASE_URL_MAIN + 'vux-resources/';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSettings, "GET_DEFAULT_SOLUTION", {
        get: function () {
            if (AppSettings.MOCK) {
                return AppSettings.BASE_URL_DEV + 'mock-api/solution.config.json';
                ;
            }
            return 'api/vux/defaultSolution';
        },
        enumerable: true,
        configurable: true
    });
    AppSettings.MOCK = false;
    AppSettings.PRODUCTION = false;
    AppSettings.BASE_URL_DEV = AppSettings.MOCK ? 'http://localhost:8080/' : 'http://localhost:8081/';
    return AppSettings;
}());
exports.AppSettings = AppSettings;
