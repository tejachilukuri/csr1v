var samplesMapUtils = {
   setStyle: function (feature) {
       return {
           icon: {
               path: 0,
               strokeColor: "green",
               fillColor: feature.getProperty("markerCol"),
               fillOpacity: 1,
               scale: 10,
               strokeWeight: 1
           }
       }
   }
};