"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppUtil = (function () {
    function AppUtil() {
    }
    AppUtil.prototype.getShortenedAppDescription = function (description) {
        var maxLength = 75;
        var ellipsestext = '...';
        if (description && description.length > maxLength) {
            var shortenedText = description.substr(0, maxLength);
            var fullText = description.substr(maxLength, description.length - maxLength);
            return shortenedText + ellipsestext;
        }
        else {
            return description;
        }
    };
    AppUtil.prototype.getAppDescription = function (description, id, length) {
        var maxLength = length ? length : 100;
        var ellipsestext = '<a href="javascript:;">Read More</a>';
        var showLessText = '<a href="javascript:;">Show Less</a>';
        var isExpand = false;
        if (webix.$$(id)) {
            isExpand = webix.$$(id).config.label.indexOf(ellipsestext) > -1 ? true : false;
        }
        if (description && description.length > maxLength) {
            if (isExpand) {
                return description + showLessText;
            }
            else {
                var shortenedText = description.substr(0, maxLength);
                return shortenedText + ellipsestext;
            }
        }
        else {
            return description;
        }
    };
    AppUtil.prototype.getShortDescription = function (description, length) {
        var maxLength = length ? length : 100;
        var ellipsestext = '...';
        if (description && description.length > maxLength) {
            var shortenedText = description.substr(0, maxLength);
            var fullText = description.substr(maxLength, description.length - maxLength);
            return shortenedText + ellipsestext;
        }
        else {
            return description;
        }
    };
    AppUtil.prototype.showHideToolbar = function (isShow) {
        if (document.getElementById('home-toolbar')) {
            document.getElementById('home-toolbar').style.display = isShow ? 'block' : 'none';
        }
    };
    AppUtil.prototype.showHideBanner = function (isShow) {
        if (document.getElementById('banner')) {
            document.getElementById('banner').style.display = isShow ? 'block' : 'none';
        }
    };
    AppUtil.prototype.showHideCart = function (isShow) {
        if (document.getElementById('appstoreCart')) {
            document.getElementById('appstoreCart').style.display = isShow ? 'block' : 'none';
        }
    };
    AppUtil.prototype.getShortTitle = function (title, length) {
        var maxLength = length ? length : 24;
        var ellipsestext = '...';
        if (title && title.length > maxLength) {
            var shortenedText = title.substr(0, maxLength);
            var fullText = title.substr(maxLength, title.length - maxLength);
            return shortenedText + ellipsestext;
        }
        else {
            return title;
        }
    };
    AppUtil = __decorate([
        core_1.Injectable()
    ], AppUtil);
    return AppUtil;
}());
exports.AppUtil = AppUtil;
