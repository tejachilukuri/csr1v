"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var app_settings_1 = require("../app.settings");
var core_1 = require("@angular/core");
var ApiUtil = (function () {
    function ApiUtil() {
    }
    ApiUtil.prototype.transformSolution = function (form, apps, callback) {
        vulcanuxcore_1.BaseVUXService.getInstance().get(app_settings_1.AppSettings.APP_SOLUTIONREQUEST).subscribe(function (res) {
            var solution = res;
            solution['name'] = form.solutionName;
            solution['displayName']["value"] = form.solutionDisplayName;
            solution['description']["value"] = form.description;
            solution['iconUrl']["value"] = form.iconUrl;
            if (form.solutionId) {
                solution['id'] = form.solutionId;
            }
            apps.map(function (item) {
                solution["components"][0]["components"].push({
                    "@class": "com.apporchid.vulcanux.ui.config.application.ApplicationReferenceConfig",
                    "componentCategory": "ApplicationReference",
                    "id": item.id
                });
            });
            callback(solution);
        });
    };
    ApiUtil = __decorate([
        core_1.Injectable()
    ], ApiUtil);
    return ApiUtil;
}());
exports.ApiUtil = ApiUtil;
